const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');
const fs = require('fs')
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

const cleanEnv = (value, fallback = '') => (value || fallback).trim();

const host = cleanEnv(process.env.HOST || 'localhost');
const port = Number(cleanEnv(process.env.PORT || '3003'));

const baseURL = 'http://localhost:5000';

// Export a function in order to change the behavior according the `mode` variable
module.exports = (env, argv) => {
  const mode = argv ? argv.mode : 'production';

  const distPath = path.join(__dirname, 'ui-dist');
  const srcPath = path.join(__dirname, 'src');
  const testsPath = path.join(__dirname, 'tests');

  const config = {
    entry: ['@babel/polyfill', './src/index.jsx'],
    mode,
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
        },
        {
          test: /\.svg$/,
          use: [{ loader: 'svg-inline-loader' }],
        },
        {
          test: /\.(js|jsx)$/,
          include: [srcPath],
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-react'],
              plugins: ['@babel/plugin-proposal-object-rest-spread'],
            },
          },
        },
        {
          test: /\.(js|jsx)$/,
          include: [srcPath],
          enforce: 'pre',
          loader: 'eslint-loader',
        },
      ],
    },
    output: {
      filename: 'bundle.js',
      path: distPath,
    },
    optimization: {
      minimizer: [new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 6,
        },
      })],
    },
    plugins: [new CleanWebpackPlugin()],
    devtool: 'inline-source-map',
    resolve: {
      extensions: ['*', '.js', '.jsx'],
      modules: ['node_modules'],
      alias: {
        react: path.resolve('./node_modules/react'),
        'rtc-cafe-client': path.resolve('./node_modules/rtc-cafe-client'),
      },
    }
  };

  if (mode === 'development') {
    Object.assign(config, {
      // https://github.com/webpack-contrib/karma-webpack#source-maps
      // https://github.com/airbnb/enzyme/blob/master/docs/guides/karma.md
      devtool: 'inline-source-map',
    });

    config.optimization.minimize = false;
  }
  return config;
};