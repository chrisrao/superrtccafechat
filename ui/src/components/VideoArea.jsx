import React, { useEffect, useRef, useContext, useState } from 'react';

import AppContext from '../AppContext';
import { useSize } from '../hooks';
import { usePeerMediaTracks } from '../media-hooks';
import PeerVideo from './PeerVideo';
import { Button } from './ui-components';


const MIN_VIDEO_SIZE = 180;
const BUTTON_SIZE = 50;

const hasCameraTracks = (peerTrackDefsForSid) => {
  for (const id in peerTrackDefsForSid) {
    const { description } = peerTrackDefsForSid[id];
    if (description === 'camera') {
      return true;
    }
  }
};

const getNumPeerTracks = (peerTrackDefs) => {
  let tracks = 0;
  for (const sid in peerTrackDefs) {
    const peerTrackDefsForSid = peerTrackDefs[sid];
    if (hasCameraTracks(peerTrackDefsForSid)) {
      tracks += 1;
    }
  }
  return tracks;
};

const getNumRows = (numPeerTracks, columns) => Math.ceil(numPeerTracks / columns);

const getVideoGridTemplateColumns = (columns) => {
  const arr = [];
  for (let i=0; i < columns; i += 1) {
    arr.push('auto');
  }
  return arr.join(' ');
};

const computeSinglePageDimensions = (maxWidth, maxHeight, numPeerTracks) => {
  let columns = 1;
  let size = 0;
  while (true) {
    size = Math.min(maxWidth / columns, maxHeight);
    if (size < MIN_VIDEO_SIZE) {
      return [null, null];
    }
    const rows = getNumRows(numPeerTracks, columns);
    if (rows * size < maxHeight || columns === numPeerTracks) {
      break;
    }
    columns += 1;
  }
  return [columns, size];
};

const computePaginatedDimensions = (containerWidth, containerHeight, numPeerTracks) => {
  if (numPeerTracks === 0) {
    return [0, 1, 0];
  }
  let maxWidth = containerWidth - 20;
  let maxHeight = containerHeight - 20;
  for (let tracksPerPage=numPeerTracks; tracksPerPage > 0; tracksPerPage -= 1) {
    let [columns, size] = computeSinglePageDimensions(maxWidth, maxHeight, tracksPerPage);
    if (columns !== null) {
      return [tracksPerPage, columns, size];
    }
    if (tracksPerPage === numPeerTracks) {
      if (maxWidth > maxHeight) {
        // We're putting the page buttons on the left and right
        maxWidth -= BUTTON_SIZE*2;
      } else {
        // We're putting the page buttons on the top and button
        maxHeight -= BUTTON_SIZE*2;
      }
    }
  }
  if (maxWidth > maxHeight) {
    return [1, 1, maxHeight];
  }
  return [1, 1, maxWidth];
};

const VideoArea = () => {
  const containerRef = useRef(null);
  const { peerTracks, peerTrackDefs } = useContext(AppContext);
  const [containerWidth, containerHeight] = useSize(containerRef);
  const maxWidth = containerWidth - 20;
  const maxHeight = containerHeight - 20;
  const numPeerTracks = getNumPeerTracks(peerTrackDefs);
  const [tracksPerPage, columns, size] = (
    computePaginatedDimensions(containerWidth, containerHeight, numPeerTracks)
  );
  const [page, setPage] = useState(0);
  const wide = containerWidth > containerHeight;
  const multiPage = tracksPerPage < numPeerTracks;
  const buttonGridTemplate = `${BUTTON_SIZE}px 1fr ${BUTTON_SIZE}px`;

  useEffect(() => {
    setPage(0);
  }, [tracksPerPage]);

  const renderMedia = () => {
    const result = [];
    const sortedSids = Object.keys(peerTracks).sort();
    for (let index=page*tracksPerPage; index < (page + 1)*tracksPerPage; index += 1) {
      const sid = sortedSids[index];
      if (!hasCameraTracks(peerTrackDefs[sid])) {
        continue;
      }
      result.push(
          <div
            key={`${sid}`}
            style={{ display: 'flex', flexFlow: 'column', flexGrow: 1, alignItems: 'center' }}
          >
            <div style={{ flexGrow: 1, display: 'inline-block', width: size, height: size }}>
              <PeerVideo sid={sid} filter={['camera']} includeName={true} />
            </div>
          </div>
      );
    }
    return result;
  };

  const renderInterfaceContents = () => {
    const results = [];

    if (multiPage) {
      if (page > 0) {
        results.push(<Button onClick={() => setPage(page-1)}>O</Button>);
      } else {
        results.push(<div />);
      }
    }

    results.push(
      <div
        style={{
          display: 'grid',
          height: '100%',
          gridTemplateColumns: getVideoGridTemplateColumns(columns),
        }}
      >
        {renderMedia()}
      </div>
    );

    if (multiPage) {
      if ((page+1)*tracksPerPage < numPeerTracks) {
        results.push(<Button onClick={() => setPage(page+1)}>O</Button>);
      } else {
        results.push(<div />);
      }
    }

    return results;
  };

  return (
    <div
      ref={containerRef}
      style={{
        background: 'black',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
      }}
    >
      <div
        style={{
          display: 'grid',
          width: '100%',
          height: '100%',
          gridTemplateColumns: (multiPage && wide) ? buttonGridTemplate : '1fr',
          gridTemplateRows: (multiPage && !wide) ? buttonGridTemplate : '1fr',
        }}
      >
        {renderInterfaceContents()}
      </div>
    </div>
  );
};

export default VideoArea;
