import React from 'react';
import styled from 'styled-components';

import { generateStyleProps } from './style-helpers';


const DEFAULT_BG_COLOR = {
  default: 'transparent',
  hover: 'rgba(70, 70, 70, 0.4)',
  active: 'rgba(70, 70, 70, 0.2)',
};
const DEFAULT_BORDER_COLOR = 'white';
const DEFAULT_FONT_COLOR = 'white';

const Button = styled.button`
  padding: 5px;
  margin: 5px;
  border-width: 1px;
  border-style: solid;
  border-radius: 3px;
  cursor: pointer;
  ${({ bgColor }) => generateStyleProps('background-color', bgColor || DEFAULT_BG_COLOR)}
  ${({ borderColor }) => generateStyleProps('border-color', borderColor || DEFAULT_BORDER_COLOR)}
  ${({ fontColor }) => generateStyleProps('color', fontColor || DEFAULT_FONT_COLOR)}
`;

export default Button;
