import React, { useState, useEffect, useCallback } from 'react';

const getSize = (ref) => {
  if (ref && ref.current) {
    return [ref.current.offsetWidth, ref.current.offsetHeight];
  }
  return [0, 0];
};

export const useSize = (ref) => {
  const [size, setSize] = useState(getSize(ref));

  useEffect(() => {
    const resizeObserver = new ResizeObserver(() => setSize(getSize(ref)));
    if (ref && ref.current) {
      resizeObserver.observe(ref.current)
    }
    return () => resizeObserver.disconnect();
  }, [ref]);

  return size;
};
