import React, { useContext, useState } from 'react';

import AppContext from '../../AppContext';
import { Button, Tooltip } from '../ui-components';

const Header = () => {
  const { setSession, doRequest } = useContext(AppContext);
  const [showingTooltip, setShowingTooltip] = useState(false);

  const onLogoutButtonClick = () => (
    doRequest('DELETE', '/sessions/current')
      .then(() => setSession(null))
  );

  const onCopyRoomUrlButtonClick = () => {
    const $body = document.getElementsByTagName('body')[0];
    const $tempInput = document.createElement('INPUT');
    $body.appendChild($tempInput);
    $tempInput.setAttribute('value', document.location.href);
    $tempInput.select();
    document.execCommand('copy');
    $body.removeChild($tempInput);
    setShowingTooltip(true);
    setTimeout(() => setShowingTooltip(false), 2000);
  };

  return (
    <header style={{
      background: 'darkgrey',
      padding: 10,
      textAlign: 'right',
    }}>
      <div style={{ display: 'inline-block', position: 'relative' }}>
        <Button onMouseUp={onCopyRoomUrlButtonClick}>Copy Room Link</Button>
        {showingTooltip && <Tooltip>Copied Room Link!</Tooltip>}
      </div>
      <Button onMouseUp={onLogoutButtonClick}>Log Out</Button>
    </header>
  );
};

export default Header;
