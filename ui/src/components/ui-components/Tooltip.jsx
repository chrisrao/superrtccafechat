import React from 'react';
import styled from 'styled-components';

const DEFAULT_BG_COLOR = 'orange';
const DEFAULT_FONT_COLOR = 'white';

const _TooltipBody = styled.div`
  background-color: ${({ bgColor }) => bgColor || DEFAULT_BG_COLOR};
  padding: 5px;
  border-radius: 5px;
  font-size: 0.8em;
  white-space: nowrap;
  color: ${({ fontColor }) => fontColor || DEFAULT_FONT_COLOR};
`;

const _TooltipArrow = styled.div`
  border-width: 0px 5px 10px 5px;
  border-style: solid;
  margin-left: calc(50% - 5px);
  width: 0px;
  height: 0px;
  border-color: transparent transparent ${({ color }) => color || DEFAULT_BG_COLOR} transparent;
`;

const Tooltip = ({ bgColor, fontColor, children, ...props }) => {
  return (
    <div style={{ position: 'relative' }}>
      <div style={{ position: 'absolute', top: '100%', zIndex: 2 }}>
        <_TooltipArrow color={bgColor} />
        <_TooltipBody bgColor={bgColor} fontColor={fontColor} {...props}>{children}</_TooltipBody>
      </div>
    </div>
  );
};

export default Tooltip;
