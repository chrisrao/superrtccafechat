import React, { useState, useEffect, useRef, useCallback } from 'react';
import { Router } from 'react-router-dom';
import { useRouteMatch } from 'react-router-dom';
import { useRTCCafe, useRTCCafeSignaller } from 'rtc-cafe-react';
import { v4 as uuidv4 } from 'uuid';

import AppContext from './AppContext';
import { useMediaTrack, useScreenTrack } from './media-hooks';


const PEER_INFO = 'PEER-INFO';
const DEBUG_MESSAGE = 'DEBUG-MESSAGE';

const useMessages = () => {
  const defaultOptions = {
    flash: true,
    level: 'info',
    timeout: 4000,
  };
  const [messages, setMessages] = useState([]);

  const removeMessage = (id) => {
    const index = messages.findIndex(({ id: itemId }) => itemId === id);
    const { interval } = messages[index];
    messages.splice(index, 1);
    setMessages(messages);
    if (interval) {
      clearInterval(interval);
    }
  };

  const addMessage = (message, opts = {}) => {
    const { flash, level, timeout } = Object.assign({}, defaultOptions, opts);
    const id = uuidv4();
    const messageListing = { message, flash, level, id };
    if (flash) {
      messageListing.interval = setInterval(() => removeMessage(id), timeout);
    }
    messages.push(messageListing);
    setMessages(messages);
    return messageListing;
  };

  const sanitizedMessages = messages.map(({ message, level, id }) => { message, level, id });

  return [sanitizedMessages, addMessage, removeMessage];
};

const generateCameraTrack = () => (
  navigator.mediaDevices.getUserMedia({ video: true }).then(stream => stream.getTracks()[0])
);

const generateMicrophoneTrack = () => (
  navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => stream.getTracks()[0])
);

const generateScreenTrack = () => (
  navigator.mediaDevices.getDisplayMedia().then(stream => stream.getTracks()[0])
);

const AppContextProvider = ({ baseUrl, children }) => {
  const [muted, setMuted] = useState(true);
  const [session, setSession] = useState(null);
  const [needsSessionCheck, setNeedsSessionCheck] = useState(true);
  const [peerInfo, setPeerInfo] = useState({});
  const [screenSid, setScreenSid] = useState(null);
  const roomMatch = useRouteMatch("/room/:roomId");
  const [messages, addMessage, removeMessage] = useMessages();
  const room = roomMatch ? roomMatch.params.roomId : null;
  const { signaller, connect: signallerConnect, disconnect: signallerDisconnect, state: rtcCafeSignallerState } = useRTCCafeSignaller({ host: 'https://superrtccafechat.herokuapp.com' });
  const { client: rtcCafeClient, state: rtcCafeState, joinRoom, disconnect } = useRTCCafe({ room, signaller });
  const [cameraTrack, cameraOn, toggleCameraTrack] = (
    useMediaTrack(generateCameraTrack, "camera", addMessage)
  );
  const [microphoneTrack, microphoneOn, toggleMicrophoneTrack] = (
    useMediaTrack(generateMicrophoneTrack, "microphone", addMessage)
  );
  const [screenTrack, screenOn, toggleScreenTrack] = useScreenTrack(screenSid, addMessage);

  const onConnectToPeer = useCallback((sid) => {
    return rtcCafeClient.sendMessage(
      { type: PEER_INFO, data: { name: session.name } }, sid,
    );
  }, [rtcCafeClient, session]);

  const onPeerInfoMessage = (id, data) => {
    const newPeerInfo = Object.assign({}, peerInfo, { [id]: data });
    setPeerInfo(newPeerInfo);
  };

  const onMessage = useCallback((id, message) => {
    const { type, data } = message;
    if (type === PEER_INFO) {
      onPeerInfoMessage(id, data);
    } else if (type === DEBUG_MESSAGE) {
      console.log(DEBUG_MESSAGE, id, data);
    }
  }, [peerInfo]);

  const onReassignPeerSid = useCallback((oldSid, sid) => {
    const newPeerInfo = Object.assign({}, peerInfo, { [sid]: peerInfo[oldSid] });
    delete newPeerInfo[oldSid];
    setPeerInfo(newPeerInfo);
  }, [peerInfo]);

  const onDisconnectFromPeer = useCallback((id) => {
    const newPeerInfo = Object.assign({}, peerInfo);
    delete newPeerInfo[id];
    setPeerInfo(newPeerInfo);
  }, []);

  let peerTrackDefs = {};
  let peerTracks = {};
  if (rtcCafeClient) {
    peerTrackDefs = rtcCafeClient.getPeerTrackDefinitions();
    peerTracks = rtcCafeClient.getPeerTracks();
  }

  const cleanupRtcCafeClient = () => {
    if (!rtcCafeClient) {
      return;
    }

    rtcCafeClient.off('connectToPeer', onConnectToPeer);
    rtcCafeClient.off('message', onMessage);
    rtcCafeClient.off('reassignPeerSid', onReassignPeerSid);
    rtcCafeClient.off('disconnectFromPeer', onDisconnectFromPeer);

    if (session || needsSessionCheck) {
      return;
    }

    disconnect();
    if (cameraOn) {
      toggleCameraTrack();
    }
    if (microphoneOn) {
      toggleMicrophoneTrack();
    }
    if (screenOn) {
      toggleScreenTrack();
    }
  };

  const cleanupRtcCafeSignaller = () => {
    if (session || needsSessionCheck) {
      return;
    }
    if (signaller) {
      signallerDisconnect();
    }
  };

  useEffect(() => {
    if (!room) {
      return cleanupRtcCafeSignaller;
    }
    signallerConnect();
    return cleanupRtcCafeSignaller;
  }, [rtcCafeSignallerState, room, session, needsSessionCheck]);

  useEffect(() => {
    if (!room) {
      return cleanupRtcCafeClient;
    }
    if (rtcCafeSignallerState === 'connected') {
      joinRoom();
    }
    return cleanupRtcCafeClient;
  }, [rtcCafeSignallerState, room, session, needsSessionCheck, cameraOn, microphoneOn, screenOn]);

  useEffect(() => {
    if (!rtcCafeClient) {
      return cleanupRtcCafeClient;
    }

    window.myRTCCafeCient = rtcCafeClient;
    window.sendDebugMessage = (message) => {
      window.myRTCCafeCient.sendMessage({ type: DEBUG_MESSAGE, data: { message } })
    };

    rtcCafeClient.on('connectToPeer', onConnectToPeer);
    rtcCafeClient.on('message', onMessage);
    rtcCafeClient.on('reassignPeerSid', onReassignPeerSid);
    rtcCafeClient.on('disconnectFromPeer', onDisconnectFromPeer);

    return cleanupRtcCafeClient;
  }, [rtcCafeClient, session, peerInfo, onConnectToPeer, onMessage, onReassignPeerSid, onDisconnectFromPeer]);

  useEffect(() => {
    for (const sid in peerTrackDefs) {
      if (!rtcCafeClient.connectedPeers.includes(sid)) {
        console.error('sid not a real peer', sid);
        continue;
      }
      for (const id in peerTrackDefs[sid]) {
        rtcCafeClient.requestPeerTrack(sid, id);
      }
    }
  }, [peerTrackDefs]);

  useEffect(() => {
    for (const sid in peerTracks) {
      const peerTrackDefsForSid = peerTrackDefs[sid];
      const peerTracksForSid = peerTracks[sid];
      for (const id in peerTracksForSid) {
        const { description } = peerTrackDefsForSid[id];
        if (description === 'screen') {
          setScreenSid(sid);
          return;
        }
      }
    }
    setScreenSid(null);
  }, [peerTracks]);

  const unmute = () => {
    setMuted(false);
  };

  const doRequest = async (method, path, body, headers = {}) => {
    const fetchParams = { method, headers };
    if (['POST', 'PUT', 'PATCH'].includes(method)) {
      fetchParams.body = JSON.stringify(body);
      fetchParams.headers['Content-Type'] = 'application/json';
    }
    return fetch(`${baseUrl}${path}`, fetchParams)
      .then((response) => {
        if (response.ok && response.status === 204) {
          return true;
        }
        return response.json().then((responseBody) => {
          if (!response.ok) {
            throw responseBody;
          }
          return responseBody;
        });
      });
  };

  return (
    <AppContext.Provider
      value={{
        session,
        setSession,
        needsSessionCheck,
        setNeedsSessionCheck,
        toggleCameraTrack,
        toggleMicrophoneTrack,
        toggleScreenTrack,
        cameraOn,
        microphoneOn,
        screenOn,
        screenSid,
        peerInfo,
        messages,
        addMessage,
        removeMessage,
        rtcCafeClient,
        rtcCafeState,
        peerTracks,
        peerTrackDefs,
        unmute,
        muted,
        doRequest,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;
