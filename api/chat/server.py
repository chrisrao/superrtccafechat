import os

import flask
from flask_socketio import SocketIO
from rtc_cafe_signaller.wrappers import flask_socketio_wrapper

from chat import redis_client, sessions
from chat.decorators import require_session, require_no_session


def build():
    app = flask.Flask(__name__)
    socketio = SocketIO(app, engineio_logger=True)
    flask_socketio_wrapper.wrap(socketio)

    app.add_url_rule(
        rule="/assets/<path:path>",
        methods=["GET"],
        view_func=assets,
        provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/",
        methods=["GET"],
        view_func=landing,
        defaults={"path": ""},
        provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/<path:path>", methods=["GET"], view_func=landing, provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/api/sessions/", methods=["POST"], view_func=create_session, provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/api/sessions/current",
        methods=["GET"],
        view_func=get_session,
        provide_automatic_options=True,
    )
    app.add_url_rule(
        rule="/api/sessions/current",
        methods=["DELETE"],
        view_func=delete_session,
        provide_automatic_options=True,
    )

    return app, socketio


def landing(path):
    return flask.render_template("landing.html")


def assets(path):
    root_dir = os.path.dirname(os.getcwd())
    return flask.send_from_directory(os.path.join(root_dir, os.environ['UI_ASSET']), path, add_etags=False, cache_timeout=0)


@require_no_session(cleanup_cookies=False)
def create_session():
    name = flask.request.json["name"]
    session = sessions.create(name)
    response = flask.make_response(session, 201)
    response.set_cookie("session", session["_id"])
    return response


@require_session
def get_session(session):
    return flask.make_response(session, 200)


@require_session
def delete_session(session):
    sessions.delete(session["_id"])
    response = flask.make_response("", 204)
    response.set_cookie("session", "", max_age=None)
    return response