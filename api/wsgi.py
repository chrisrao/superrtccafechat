import logging
import os

import eventlet

from chat import server

dev = bool(os.environ.get("DEV", "false"))
if dev:
  print('DEV')

eventlet.monkey_patch()

app, socketio = server.build()

app.logger.setLevel(logging.INFO)

logging.getLogger('socketIO-client').setLevel(logging.INFO)
logging.basicConfig()

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=int(os.environ.get('PORT', 5000)), log_output=True, debug=dev, use_reloader=dev)
