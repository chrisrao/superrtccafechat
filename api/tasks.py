import os
import time

from invoke import task


@task()
def run(context):
  context.run('gunicorn --worker-class eventlet --no-sendfile --access-logfile=- -w 1 wsgi:app')
  # context.run('python wsgi.py')
  # context.run('uwsgi --protocol=http --socket 0.0.0.0:{} wsgi:socketio'.format(os.environ['PORT']))
