import React, { useContext, useRef } from 'react';
import { useHistory } from 'react-router-dom';

import AppContext from '../AppContext';


const getNextParam = () => {
  const match = document.location.search.match(/next=([^&$]+)/);
  if (!match) {
    return null;
  }
  return decodeURI(match[1]);
};

const SignIn = () => {
  const { setSession, doRequest } = useContext(AppContext);
  const inputRef = useRef(null);
  const history = useHistory();

  const onSetNameButtonClick = () => (
    doRequest('POST', '/sessions/', { name: inputRef.current.value })
      .then((response) => setSession(response))
  );

  return (
    <div>
      <div>
        Set Name
      </div>
      <div>
        <input ref={inputRef} type="text" />
        <button onClick={onSetNameButtonClick}>Send</button>
      </div>
    </div>
  );
};

export default SignIn;
