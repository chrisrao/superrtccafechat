import React, { useContext, useEffect, useRef, useCallback, useState } from 'react';

import AppContext from '../AppContext';


const getTracks = (sid, filter) => {
  const tracks = [];
  const { peerTracks, peerTrackDefs } = useContext(AppContext);
  const peerTrackDefsForSid = peerTrackDefs[sid];
  const peerTracksForSid = peerTracks[sid];
  for (const id in peerTrackDefsForSid) {
    const { description } = peerTrackDefsForSid[id];
    if (peerTracksForSid[id] && filter.includes(description)) {
      tracks.push(peerTracksForSid[id]);
    }
  }
  return tracks;
};

const PeerVideo = ({ sid, filter, includeName, ...videoProps }) => {
  const svgRef = useRef(null);
  const videoRef = useRef(null);
  const [svgWidth, setSvgWidth] = useState(0);
  const tracks = getTracks(sid, filter);
  const { muted, peerInfo } = useContext(AppContext);
  const { name } = peerInfo[sid] || {};

  useEffect(() => {
    if (!videoRef || !videoRef.current) {
      return;
    }
    const stream = new MediaStream();
    for (const track of tracks) {
      stream.addTrack(track);
    }
    videoRef.current.srcObject = stream;
  }, [videoRef, tracks]);

  useEffect(() => {
    if (!svgRef || !svgRef.current) {
      return;
    }
    const bbox = svgRef.current.getBBox();
    setSvgWidth(bbox.width);
  }, [svgRef, name]);

  return (
    <div style={{ position: 'relative', overflow: 'hidden' }}>
      <video
        autoPlay
        muted={muted}
        ref={videoRef}
        style={{ width: '100%', height: '100%' }}
        {...videoProps}
      />
      {
        includeName && (
          <svg height="30px" ref={svgRef} style={{ position: 'absolute', bottom: 0, left: '50%', transform: 'translateX(-50%)', width: svgWidth }}>
            <g>
              <text lengthAdjust="spacingAndGlyphs" height="20px" y="20px" fill="white">
                {name}
              </text>
            </g>
          </svg>
        )
      }
    </div>
  );
};

export default PeerVideo;
