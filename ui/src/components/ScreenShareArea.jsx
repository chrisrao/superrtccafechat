import React from 'react';

import PeerVideo from './PeerVideo';


const ScreenShareArea = ({ sid }) => (
  <div style={{ position: 'relative' }}>
    <div style={{ position: 'absolute', width: '100%', height: '100%' }}>
      <PeerVideo sid={sid} filter={['screen']} />
    </div>
  </div>
);

export default ScreenShareArea;
