import os

import redis

_REDIS_URL = os.environ.get("REDIS_URL")
_REDIS_CLIENT = redis.from_url(_REDIS_URL)


def get(key):
    return _REDIS_CLIENT.get(key)


def set(key, value, **kwargs):
    _REDIS_CLIENT.set(key, value, **kwargs)


def delete(key):
    _REDIS_CLIENT.delete(key)
