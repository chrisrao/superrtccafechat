import React, { useContext } from 'react';

import AppContext from '../../AppContext';
import { Button } from '../ui-components';

const STREAM_BUTTON_BG_ON = {
  default: 'rgba(70, 70, 70, 0.4)',
  hover: 'rgba(70, 70, 70, 0.6)',
  active: 'rgba(70, 70, 70, 0.5)',
};

const STREAM_BUTTON_BG_OFF = {
  default: 'transparent',
  hover: 'rgba(70, 70, 70, 0.4)',
  active: 'rgba(70, 70, 70, 0.2)',
};

const Footer = () => {
  const {
    toggleCameraTrack,
    toggleMicrophoneTrack,
    toggleScreenTrack,
    cameraOn,
    microphoneOn,
    screenOn,
    screenSid,
    rtcCafeState,
  } = useContext(AppContext);

  return (
    <footer style={{
      background: 'darkgrey',
      padding: 10,
    }}>
      <Button
        bgColor={cameraOn ? STREAM_BUTTON_BG_ON : STREAM_BUTTON_BG_OFF}
        onClick={toggleCameraTrack}
      >
        Camera
      </Button>
      <Button
        bgColor={microphoneOn ? STREAM_BUTTON_BG_ON : STREAM_BUTTON_BG_OFF}
        onClick={toggleMicrophoneTrack}
      >
        Microphone
      </Button>
      {!screenSid && (
        <Button
          bgColor={screenOn ? STREAM_BUTTON_BG_ON : STREAM_BUTTON_BG_OFF}
          onClick={toggleScreenTrack}
        >
          Screen
        </Button>
      )}
    </footer>
  );
};

export default Footer;
