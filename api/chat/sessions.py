import uuid

from chat import redis_client

_SESSION_EXPIRY = 30 * 60


def _get_session_object(session_id):
    name = redis_client.get(session_id)
    if name:
        return dict(_id=session_id, name=name.decode("utf-8"))


def create(name):
    session_id = f"session_{str(uuid.uuid4())}"
    redis_client.set(session_id, name, ex=_SESSION_EXPIRY)
    return _get_session_object(session_id)


def get(session_id):
    return _get_session_object(session_id)


def delete(session_id):
    redis_client.delete(session_id)
