import React, { useContext } from 'react';

import AppContext from '../AppContext';
import { useSize } from '../hooks';
import PeerVideo from './PeerVideo';


const AudioArea = () => {
  const { peerTracks, peerTrackDefs } = useContext(AppContext);

  const renderMedia = () => {
    const result = [];
    for (const sid in peerTracks) {
      const peerTracksForSid = peerTracks[sid];
      const peerTrackDefsForSid = peerTrackDefs[sid];
      for (const id in peerTracksForSid) {
        const { description } = peerTrackDefsForSid[id];
        if (description !== 'microphone') {
          continue;
        }
        result.push(<PeerVideo key={sid} sid={sid} filter={['microphone']} />);
      }
    }
    return result;
  };

  return (
    <div style={{ width: 0, height: 0 }}>
      {renderMedia()}
    </div>
  );
};

export default AudioArea;
