import React, { useContext } from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { v4 as uuidv4 } from 'uuid';

import AppContext from '../../AppContext';
import { Button } from '../ui-components';
import Footer from './Footer';
import Header from './Header';
import UnmuteHeader from './UnmuteHeader';
import Screen from '../Screen';


const redirectPath = () => {
  const match = document.location.search.match(/next=([^&$]+)/);
  if (match) {
    return decodeURI(match[1]);
  }
  return `/room/${uuidv4()}`;
};

const SessionContainer = () => {
  const { unmute, muted } = useContext(AppContext);
  const gridTemplateRows = muted && 'auto auto minmax(0, 1fr) auto' || 'auto minmax(0, 1fr) auto';

  return (
    <div style={{ display: 'grid', gridTemplateRows: gridTemplateRows, height: '100%' }}>
      <Header />
      {muted && <UnmuteHeader />}
      <div style={{ position: 'relative' }}>
        <Switch>
          <Route path="/room/:roomId" component={Screen} />
          <Redirect to={redirectPath()} />
        </Switch>
      </div>
      <Footer />
    </div>
  );
};

export default SessionContainer;
