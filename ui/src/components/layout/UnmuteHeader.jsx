import React, { useContext } from 'react';

import AppContext from '../../AppContext';
import { Button } from '../ui-components';


const UnmuteHeader = () => {
  const { unmute } = useContext(AppContext);
  return (
    <header style={{
      background: 'wheat',
      padding: 10,
      textAlign: 'center',
    }}>
      <Button
        bgColor={{
          default: 'red',
          hover: 'darkred',
          active: 'darkred',
        }}
        onClick={unmute}
      >
        Allow Audio
      </Button>
    </header>
  );
};

export default UnmuteHeader;
