import React from 'react';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import { render } from 'react-dom';

import App from './components/layout/App';
import AppContextProvider from './AppContextProvider';

import './main.css';


const containerElement = document.getElementById('app-container');
const baseUrl = containerElement.getAttribute('data-base-url');

render(
  <Router>
    <AppContextProvider baseUrl={baseUrl}>
      <App />
    </AppContextProvider>
  </Router>,
  containerElement,
);
