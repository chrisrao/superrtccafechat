import React from 'react';
import {
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import SignIn from '../SignIn';


const NoSessionContainer = () => (
  <Switch>
    <Route path="/setname" component={SignIn} />
    <Redirect to={`/setname?next=${encodeURI(document.location.pathname)}`} />
  </Switch>
);

export default NoSessionContainer;
