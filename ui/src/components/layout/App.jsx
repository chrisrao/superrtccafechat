import React, { useContext, useEffect } from 'react';

import AppContext from '../../AppContext';
import SessionContainer from './SessionContainer';
import NoSessionContainer from './NoSessionContainer';


const App = () => {
  const { doRequest, session, setSession, needsSessionCheck, setNeedsSessionCheck } = useContext(AppContext);
  
  useEffect(() => {
    if (!needsSessionCheck) {
      return;
    }
    doRequest('GET', '/sessions/current')
      .then((response) => setSession(response))
      .finally(() => setNeedsSessionCheck(false));
  }, [needsSessionCheck]);

  if (needsSessionCheck) {
    return <div></div>;
  }

  if (session) {
    return <SessionContainer />;
  }

  return <NoSessionContainer />;
};

export default App;
