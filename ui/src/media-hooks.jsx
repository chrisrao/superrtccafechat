import { useState, useEffect } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useRTCCafe } from 'rtc-cafe-react';


export const useMediaTrack = (generateTrack, trackDescription, addMessage) => {
  const roomMatch = useRouteMatch("/room/:roomId");
  const room = roomMatch ? roomMatch.params.roomId : null;
  const { client: rtcCafeClient, state: rtcCafeState } = useRTCCafe({ room });
  const [track, setTrack] = useState(null);
  const [trackOn, setTrackOn] = useState(false);

  useEffect(() => {
    if (!rtcCafeClient || !track) {
      return;
    }
    if (trackOn) {
      rtcCafeClient.addTrack(track, trackDescription);
    } else {
      rtcCafeClient.removeTrack(track.id);
    }
  }, [trackOn, track]);

  const addTrack = () => (
    generateTrack().then(track => {
      setTrack(track);
      track.onended = () => setTrackOn(false);
    }).catch((error) => {
      console.error(error);
    })
  );

  const toggleTrack = () => {
    if (rtcCafeState != 'connected') {
      return;
    }
    setTrackOn(!trackOn);
    if (!track) {
      addTrack().catch(() => {
        addMessage(`Could not capture media from ${trackDescription}.`, { level: 'error' });
        setTrackOn(false);
      });
    }
  };

  return [track, trackOn, toggleTrack];
};

export const useScreenTrack = (screenSid, addMessage) => {
  const roomMatch = useRouteMatch("/room/:roomId");
  const room = roomMatch ? roomMatch.params.roomId : null;
  const { client: rtcCafeClient, state: rtcCafeState } = useRTCCafe({ room });
  const [track, setTrack] = useState(null);
  const [trackOn, setTrackOn] = useState(false);

  useEffect(() => {
    if (!rtcCafeClient || !track) {
      return;
    }
    if (trackOn) {
      rtcCafeClient.addTrack(track, 'screen');
    } else {
      rtcCafeClient.removeTrack(track.id);
      track.stop();
      setTrack(null);
    }
  }, [trackOn, track]);

  useEffect(() => {
    if (!screenSid) {
      return;
    }
    setTrackOn(false);
    addMessage(`Peer "${screenSid}" has control of screen share.`);
  }, [screenSid]);

  const toggleTrack = () => {
    if (rtcCafeState != 'connected') {
      return;
    }
    setTrackOn(!trackOn);
    if (!track) {
      navigator.mediaDevices.getDisplayMedia()
      .then(stream => stream.getTracks()[0])
      .then(track => {
        setTrack(track);
        track.onended = () => {
          setTrackOn(false);
        };
      })
      .catch(() => {
        addMessage('Could not capture media from screen.', { level: 'error' });
        setTrackOn(false);
      });
    }
  };

  return [track, trackOn, toggleTrack];
};

export const usePeerMediaTracks = ({ sids, descriptions }) => {
  const roomMatch = useRouteMatch("/room/:roomId");
  const room = roomMatch ? roomMatch.params.roomId : null;
  const { client: rtcCafeClient, state: rtcCafeState } = useRTCCafe({ room });
  let peerTrackDefs = {};
  let peerTracks = {};
  if (rtcCafeClient) {
    peerTrackDefs = rtcCafeClient.getPeerTrackDefinitions(sids);
    peerTracks = rtcCafeClient.getPeerTracks(sids);
  }
  const tracks = [];
  const sortedSids = Object.keys(peerTracks).sort();
  for (const sid of sortedSids) {
    const peerTrackDefsForSid = peerTrackDefs[sid];
    const peerTracksForSid = peerTracks[sid];
    const sortedIds = Object.keys(peerTracksForSid);
    for (const id of sortedIds) {
      const { description } = peerTrackDefsForSid[id];
      if (!descriptions || descriptions.includes(description)) {
        tracks.push(peerTracksForSid[id]);
      }
    }
  }
  return tracks;
};
