import json
import logging
import os
import signal
import time

from invoke import task

logger = logging.getLogger(__name__)


def _split_env(env):
    if "=" in env:
        return env.split("=", 1)
    else:
        return env, None


def _get_envvars_from_file(filename):
    if not os.path.exists(filename):
        raise RuntimeError("Couldn't find env file: {}".format(filename))
    elif not os.path.isfile(filename):
        raise RuntimeError("{} is not a file.".format(filename))
    env = {}
    with open(filename, "r") as f:
        for line in f.readlines():
            line = line.strip()
            if line and not line.startswith("#"):
                key, value = _split_env(line)
                if value:
                    value = value.format(**os.environ)
                env[key] = value
    return env


@task
def update_heroku_config(context):
    import requests
    heroku_envvars = _get_envvars_from_file("heroku.env")
    project_name = os.environ["CI_PROJECT_NAME"]
    heroku_api_key = os.environ["HEROKU_API_KEY"]
    heroku_app_url = "https://api.heroku.com/apps/{}".format(project_name)
    config_response = requests.request(
        "PATCH",
        "{}/config-vars".format(heroku_app_url),
        data=json.dumps(heroku_envvars),
        headers={
            "Accept": "application/vnd.heroku+json; version=3",
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(heroku_api_key),
        },
    )
    config_response.raise_for_status()


@task
def release_to_heroku(context, image_id, role):
    import requests
    project_name = os.environ["CI_PROJECT_NAME"]
    heroku_api_key = os.environ["HEROKU_API_KEY"]
    heroku_app_url = "https://api.heroku.com/apps/{}".format(project_name)
    release_response = requests.request(
        "PATCH",
        "{}/formation".format(heroku_app_url),
        data=json.dumps({"updates": [{"type": role, "docker_image": image_id}]}),
        headers={
            "Accept": "application/vnd.heroku+json; version=3.docker-releases",
            "Content-Type": "application/json",
            "Authorization": "Bearer {}".format(heroku_api_key),
        },
    )
    release_response.raise_for_status()
    logger.info(json.dumps(release_response.json(), indent=2))


@task
def stop_api(context):
    context.run("docker-compose stop api")


@task
def stop_ui(context):
    context.run("docker-compose stop web")


@task
def run_api(context, dockerized=False, build=True, detached=False):
    if dockerized:
        stop_api(context)
        if build:
            context.run("docker-compose build api")
        context.run("docker-compose up api {}".format('-d' if detached else ''), pty=True)
    else:
        run_infrastructure(context)
        with context.cd("api"):
            os.environ.update(_get_envvars_from_file('dev.env'))
            ui_asset_dir = os.path.join(os.getcwd(), 'ui', 'ui-dist')
            context.run("pipenv sync --dev", pty=True)
            context.run("UI_ASSET={} pipenv run invoke run".format(ui_asset_dir), pty=True)


@task
def run_ui(context, dockerized=False, build=True, detached=False):
    if dockerized:
        stop_ui(context)
        if build:
            context.run("docker-compose build web")
        context.run("docker-compose up web {}".format('-d' if detached else ''), pty=True)
    else:
        context.run("npm run --prefix=ui watch", pty=True)


@task
def run_infrastructure(context):
    context.run("docker-compose up -d $(docker-compose ps --services | grep -Ev 'web|api')")
