const generatePseudoSelectorStyleProp = (prop, pseuoSelector, value) => {
  if (pseuoSelector === "default") {
    return `${prop}: ${value};`
  }
  return `&:${pseuoSelector} {
    ${prop}: ${value};
  }`
};

export const generateStyleProps = (prop, values) => {
  if (typeof values === "string") {
    return `${prop}: ${values};`
  }
  return Object.keys(values).reduce((array, pseuoSelector) => (
    array.push(generatePseudoSelectorStyleProp(prop, pseuoSelector, values[pseuoSelector])) && array
  ), []).join("\n");
};
