import React, { useContext, useCallback } from 'react';

import AppContext from '../AppContext';
import { usePeerMediaTracks } from '../media-hooks';
import ScreenShareArea from './ScreenShareArea';
import Audio from './Audio';
import VideoArea from './VideoArea';


const Screen = () => {
  const { screenSid, muted } = useContext(AppContext);
  const screenSidFilter = screenSid ? [screenSid] : [];
  const screenTracks = usePeerMediaTracks({ sids: screenSidFilter, descriptions: ['screen'] });
  const videoTracks = usePeerMediaTracks({ descriptions: ['camera'] });
  const gridTemplate = ['1fr'];
  if (screenSid && videoTracks.length) {
    gridTemplate.push('200px');
  }

  return (
    <div>
      <video muted={muted} style={{ visibility: 'hidden', width: 0, height: 0 }}></video>
      <Audio />
      <div style={{
        display: 'grid',
        height: '100%',
        background: 'black',
        gridTemplateColumns: gridTemplate.join(' '),
      }}>
        {screenTracks.length && <ScreenShareArea sid={screenSid} /> || null}
        {videoTracks.length && <VideoArea /> || null}
      </div>
    </div>
  );
};

export default Screen;
