import functools

import flask

from chat import sessions


def require_session(view_function):
    @functools.wraps(view_function)
    def wrapper(*args, **kwargs):
        session_id = flask.request.cookies.get("session")
        if not session_id:
            return flask.make_response({"errors": ["Endpoint requires session."]}, 403)
        session = sessions.get(session_id)
        if not session:
            return flask.make_response({"errors": [f'Session id "{session_id}"" is invalid.']}, 403)
        return view_function(session, *args, **kwargs)

    return wrapper


def require_no_session(cleanup_cookies=True):
    def outer_wrapper(view_function):
        @functools.wraps(view_function)
        def inner_wrapper(*args, **kwargs):
            session_id = flask.request.cookies.get("session")
            if session_id and sessions.get(session_id):
                return flask.make_response(
                    {"errors": ["Endpoint cannot be accessed with session."]}, 403
                )
            response = view_function(*args, **kwargs)
            if cleanup_cookies:
                response.set_cookie("session", "", max_age=None)
            return response

        return inner_wrapper

    return outer_wrapper
